#!/usr/bin/env python3
import os
import sys
import subprocess
import datetime
import getpass
import paramiko

# Fonction pour demander une entrée à l'utilisateur
def prompt_user(message, default=None):
    if default:
        return input(f"{message} [{default}]: ") or default
    return input(f"{message}: ")

# Demander à l'utilisateur où placer la sauvegarde
custom_backup_dir = prompt_user("Entrez le chemin complet du répertoire de sauvegarde (laissez vide pour le chemin par défaut)")

if not custom_backup_dir:
    default_backup_dir = "/chemin/vers/repertoire_de_sauvegarde_par_defaut"
    print(f"La sauvegarde va être enregistrée dans : {default_backup_dir}")
    response = input("Appuyez sur Entrée pour continuer ou entrez un autre chemin : ")
    backup_dir = default_backup_dir if not response else response
else:
    backup_dir = custom_backup_dir

# Vérifier si le répertoire de sauvegarde existe, sinon le créer
os.makedirs(backup_dir, exist_ok=True)
timestamp = datetime.datetime.now().strftime("%Y%m%d%H%M%S")

# Lire les informations de connexion pour le serveur
server_user = input("Entrez le nom d'utilisateur du serveur : ")
server_ip = input("Entrez l'adresse IP du serveur : ")

# Lire les informations de connexion PostgreSQL
postgres_user = input("Entrez le nom d'utilisateur PostgreSQL : ")

# ... (Parties de sauvegarde des logs, vérification de bases de données, etc.)

# Demande si l'utilisateur souhaite sauvegarder d'autres fichiers
backup_other_files = input("Voulez-vous sauvegarder d'autres fichiers ? (oui/non) ").lower()

if backup_other_files == "oui":
    other_file_path = input("Entrez le chemin absolu du fichier/dossier à sauvegarder : ")
    subprocess.run(["scp", f"{server_user}@{server_ip}:{other_file_path}", f"{backup_dir}/{timestamp}/"])

# Création d'une archive sur l'ordinateur hôte
subprocess.run(["tar", "-czvf", f"{backup_dir}/backup_{timestamp}.tar.gz", f"{backup_dir}/{timestamp}"])

# Nettoyage des fichiers temporaires sur l'ordinateur hôte
subprocess.run(["rm", "-rf", f"{backup_dir}/{timestamp}"])

print(f"Sauvegarde terminée dans {backup_dir}/backup_{timestamp}.tar.gz")
