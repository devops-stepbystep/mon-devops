
# Sauvegarde auto 
Pour automatiser la sauvegarde régulière de chaque serveur, nous utiliserons des scripts Shell en conjonction avec la planification de tâches via Cron. Cela permettra d'effectuer les sauvegardes de manière automatique et périodique :

###	 Configuration de serveur mail :
Tout d'abord, il est nécessaire de mettre en place un serveur de messagerie afin de recevoir des notifications en cas d'erreurs. Pour ce faire, nous utiliserons l'outil "mutt" :
```shell
sudo apt-get install mutt
```
Ensuite, il est requis de procéder à des ajustements dans la configuration de ce service :
```shell
vi ~/.muttrc
```
	
et coller ce qui est dans .muttrc en ajoustant selon vos besoin 
Ensuite, il est nécessaire de modifier les permissions de cette configuration :
```shell
chmod 600 ~/.muttrc
```
À présent, créez le script de sauvegarde en utilisant cette commande :
```shell
vi /opt/backup.sh
```

Ce Script permet d’archiver les fichier est dossier dans /var/log/nginx /var/log/odoo /var/log/onlyoffice /var/log/auth.log /var/log/postgresql /var/log/user.log /etc/nginx /opt/mattermost/data/;
Vous pouvez changer chacun de ce chemin en fonction de vos besoins.
Pour le dump de MariaDB ou PostgreSQL, veuillez commenter ou décommenter la ligne correspondante en fonction de celle que vous souhaitez utiliser
Après avoir créé ce script, vous devez le rendre exécutable en utilisant la commande suivante :
```shell
chmod 744 backup.sh
```

Pour le lancer de manière automatique, nous allons créer une tâche cron qui l'exécutera à intervalles réguliers :
```shell
0 21 * * 1-6 /bin/bash /opt/backupdir.sh
```


```
0 : Les minutes (0 signifie le début de l'heure).
21 : L'heure (21 correspond à 21h).
* : Tous les jours du mois.
* : Tous les mois.
1-6 : Du lundi au samedi (jours de la semaine 1 à 6).
/bin/bash : Le chemin vers l'interpréteur Bash.
/opt/backupdir.sh : Le chemin vers notre script.
```

### Afin de faciliter la copie du fichier vers un serveur distant sans nécessiter de mot de passe, veuillez suivre les étapes ci-dessous 

#### Générer une paire de clés SSH :
1.	Si vous n'avez pas encore de paire de clés SSH, vous pouvez en générer une avec la commande suivante (si vous en avez déjà une, passez à l'étape 2) :
```shell
ssh-keygen -t rsa
```

Suivez les instructions pour générer la clé. Par défaut, elle sera stockée dans le répertoire **~/.ssh/id_rsa** (clé privée) et **~/.ssh/id_rsa.pub** (clé publique).

2.	Ajouter la clé publique au serveur distant :
Assurez-vous que la clé publique (~/.ssh/id_rsa.pub) soit ajoutée au fichier **~/.ssh/authorized_keys** sur le serveur distant.


Ensuite, veuillez décommenter la dernière ligne du script afin que la copie soit exécutée à chaque fois que la sauvegarde est lancée

