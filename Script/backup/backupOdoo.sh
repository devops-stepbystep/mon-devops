#!/bin/bash

# Fonction pour envoyer un e-mail en cas d'erreur
send_error_email() {
  local error_message="$1"
  local recipient="adresse_email_destination@example.com"
  local subject="Erreur lors de la sauvegarde en local le $(date +'%Y-%m-%d')"
  local attachment="/opt/err.log"

  echo -e "$error_message" | mutt -s "$subject" -a "$attachment" -- "$recipient"
  echo "E-mail d'erreur envoyé avec succès."
}

# Chemin vers le répertoire d'installation d'Odoo
ODOO_PATH="/opt/odoo"

# Répertoire de sauvegarde
BACKUP_DIR="/opt/backup"

# Nom du fichier de sauvegarde pour la base de données
DB_BACKUP_FILE="odoo_db_backup_$(date +'%Y%m%d%H%M%S').sql"

# Chemin complet pour le fichier de sauvegarde de la base de données
DB_BACKUP_PATH="$BACKUP_DIR/$DB_BACKUP_FILE"

# Création d'un fichier journal pour stocker les erreurs
ERROR_LOG="/opt/err.log"

# Initialisation du journal
echo "" > "$ERROR_LOG"

# Suppression des sauvegardes de plus d'une semaine
find "$BACKUP_DIR" -type f -name 'odoo_db_backup_*.sql' -mtime +7 -exec rm {} \;

# Sauvegarde de la base de données PostgreSQL
sudo -u postgres pg_dumpall -U postgres -f "$DB_BACKUP_PATH" || { 
  echo "Erreur lors de la sauvegarde de la base de données." >> "$ERROR_LOG"
  send_error_email "Erreur lors de la sauvegarde de la base de données."
  exit 1
}

# Sauvegarde des fichiers Odoo
tar -czvf "$BACKUP_DIR/odoo_files_backup_$(date +'%Y%m%d%H%M%S').tar.gz" "$ODOO_PATH" || { 
  echo "Erreur lors de la sauvegarde des fichiers Odoo." >> "$ERROR_LOG"
  send_error_email "Erreur lors de la sauvegarde des fichiers Odoo."
  exit 1
}

echo "Sauvegarde terminée. Base de données sauvegardée dans $DB_BACKUP_PATH."

# Vous pouvez ajouter ici des étapes de transfert vers un stockage distant si nécessaire.

exit 0
