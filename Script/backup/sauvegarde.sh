#!/bin/bash

# Demander à l'utilisateur où placer la sauvegarde
read -p "Entrez le chemin complet du répertoire de sauvegarde (laissez vide pour le chemin par défaut) : " custom_backup_dir

# Utiliser un chemin par défaut si l'utilisateur n'a rien saisi
if [ -z "$custom_backup_dir" ]; then
    default_backup_dir="/chemin/vers/repertoire_de_sauvegarde_par_defaut"
    backup_dir="$default_backup_dir"
    read -p "La sauvegarde va être enregistrée dans : $default_backup_dir. Appuyez sur Entrée pour continuer ou entrez un autre chemin et appuyez sur Entrée : "
else
    backup_dir="$custom_backup_dir"
fi

# Vérifier si le répertoire de sauvegarde existe, sinon le créer
mkdir -p "$backup_dir"
timestamp=$(date +%Y%m%d%H%M%S)

# Lire les informations de connexion pour le serveur
read -p "Entrez le nom d'utilisateur du serveur : " server_user
read -p "Entrez l'adresse IP du serveur : " server_ip

# Lire les informations de connexion PostgreSQL
read -p "Entrez le nom d'utilisateur PostgreSQL : " postgres_user

# Création du répertoire de sauvegarde
mkdir -p "$backup_dir/$timestamp"

# Sauvegarde des logs sur le serveur distant
ssh "$server_user@$server_ip" "tar -czvf /tmp/logs_$timestamp.tar.gz /chemin/vers/logs"
scp "$server_user@$server_ip:/tmp/logs_$timestamp.tar.gz" "$backup_dir/$timestamp/"

# Vérification de la présence de bases de données
db_types=()

if ssh "$server_user@$server_ip" "mysql --version > /dev/null"; then
    db_types+=("mysql")
fi

if ssh "$server_user@$server_ip" "psql --version > /dev/null"; then
    db_types+=("postgres")
fi

if ssh "$server_user@$server_ip" "echo oracle > /dev/null"; then
    db_types+=("oracle")
fi

if [ ${#db_types[@]} -eq 0 ]; then
    echo "Aucune base de données MySQL, PostgreSQL ou Oracle trouvée sur le serveur."
    exit 1
fi

echo "J'ai constaté les bases de données suivantes sur le serveur : ${db_types[*]}."

# Demander si l'utilisateur souhaite sauvegarder les bases de données
read -p "Voulez-vous sauvegarder toutes les bases de données ? (oui/non) " backup_all_response

backup_all_response_lower=$(echo "$backup_all_response" | tr '[:upper:]' '[:lower:]') # Convertir en minuscules

if [ "$backup_all_response_lower" == "oui" ]; then
    for db_type in "${db_types[@]}"; do
        read -p "Entrez le nom d'utilisateur $db_type : " db_user
        read -s -p "Entrez le mot de passe $db_type : " db_password
        echo ""

        # Création du répertoire de sauvegarde
        mkdir -p "$backup_dir/$timestamp"

        # Sauvegarde de la base de données sur le serveur distant
        if [ "$db_type" == "mysql" ]; then
            ssh "$server_user@$server_ip" "mysqldump -u $db_user -p'$db_password' nom_base_de_donnees" > "$backup_dir/$timestamp/db_backup_$db_type.sql"
        elif [ "$db_type" == "postgres" ]; then
            ssh "$server_user@$server_ip" "pg_dump -U $db_user nom_base_de_donnees" > "$backup_dir/$timestamp/db_backup_$db_type.sql"
        elif [ "$db_type" == "oracle" ]; then
            # Ajoutez la commande Oracle appropriée pour la sauvegarde ici
            ssh "$server_user@$server_ip" "rman target=/ service=orcl/mypass backup as compressed backupset of database mydb format=db_backup_1.dbf"
            exit 1
        fi

        echo "Sauvegarde de la base de données $db_type terminée dans $backup_dir/$timestamp/db_backup_$db_type.sql"
    done
else
    echo "Aucune sauvegarde de base de données ne sera effectuée."
fi

# Demande si l'utilisateur souhaite sauvegarder d'autres fichiers
read -p "Voulez-vous sauvegarder d'autres fichiers ? (oui/non) " backup_other_files

backup_other_files_lower=$(echo "$backup_other_files" | tr '[:upper:]' '[:lower:]') # Convertir en minuscules

if [ "$backup_other_files_lower" == "oui" ]; then
    read -p "Entrez le chemin absolu du fichier/dossier à sauvegarder : " other_file_path
    scp "$server_user@$server_ip:$other_file_path" "$backup_dir/$timestamp/"
fi

# Création d'une archive sur l'ordinateur hôte
tar -czvf "$backup_dir/backup_$timestamp.tar.gz" "$backup_dir/$timestamp"

# Nettoyage des fichiers temporaires sur l'ordinateur hôte
rm -rf "$backup_dir/$timestamp"

echo "Sauvegarde terminée dans $backup_dir/backup_$timestamp.tar.gz"
