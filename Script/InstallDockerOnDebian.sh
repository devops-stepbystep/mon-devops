#!/usr/bin/env bash

# Script pour installer docker-ce sur Debian 12

# Suppression de podman et buildah (s'ils sont installés)
echo "Suppression de podman et buildah..."
apt-get remove -y podman buildah

# Installation de Docker CE
echo "Installation de Docker CE..."
apt-get update
apt-get install -y ca-certificates curl gnupg lsb-release

mkdir -m 0755 -p /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/debian/gpg | gpg --dearmor -o /etc/apt/keyrings/docker.gpg

echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian \
  $(lsb_release -cs) stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null

apt-get update
apt-get install -y docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin

# Configuration du service Docker
echo "Configuration du service Docker..."
systemctl enable docker.service
systemctl start docker.service

# Vérification si Docker est en marche
if systemctl is-active --quiet docker.service; then
    echo "Docker est en marche."
else
    echo "Erreur : Docker n'est pas en marche."
    exit 1
fi

# Ajout de permissions pour l'utilisateur courant
echo "Ajout de permissions pour l'utilisateur courant..."
usermod -aG docker "$USER"

# Vérification de l'installation
echo "Vérification de l'installation..."
docker ps

# Installation de Docker Compose
echo "Installation de Docker Compose..."
apt-get install -y curl
curl -L "https://github.com/docker/compose/releases/download/v2.17.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
docker-compose --version

echo "Installation terminée !"