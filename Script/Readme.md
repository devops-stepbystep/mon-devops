# Make the script executable

```bash
chmod +x InstallDockerOnRocky.sh
```

run the script with sudo

```bash
sudo sh InstallDockerOnRocky.sh
```



# how to add `/usr/local/bin` to your `$PATH` variable:

1. Open the `.bashrc` file in your preferred text editor. You can use `nano` or `vim`:

   ```bash
   nano ~/.bashrc
   ```

2. Add the following line at the end of the `.bashrc` file to add `/usr/local/bin` to your `$PATH`:

   ```bash
   export PATH=$PATH:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/root/bin
   ```

3. Save and close the `.bashrc` file.

4. Load the changes made to the `.bashrc` file by running the following command:

   ```bash
   source ~/.bashrc
   ```