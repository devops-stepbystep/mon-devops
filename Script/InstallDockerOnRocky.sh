#!/usr/bin/env bash

# Script pour installer docker-ce sur Rocky Linux
# Exécutez avec 'sudo ./script.sh'

# Suppression de podman et buildah
echo "Suppression de podman et buildah..."
dnf remove -y podman buildah

# Installation de Docker CE
echo "Installation de Docker CE..."
dnf install -y yum-utils
yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
dnf install -y docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin

# Configuration du service Docker
echo "Configuration du service Docker..."
systemctl enable docker.service
systemctl start docker.service

# Vérification si Docker est en marche
if systemctl is-active --quiet docker.service; then
    echo "Docker est en marche."
else
    echo "Erreur : Docker n'est pas en marche."
    exit 1
fi

# Ajout de permissions pour l'utilisateur courant
echo "Ajout de permissions pour l'utilisateur courant..."
usermod -aG docker "$USER"

# Vérification de l'installation
echo "Vérification de l'installation..."
docker ps

# Installation de Docker Compose
echo "Installation de Docker Compose..."
dnf install -y curl
curl -L "https://github.com/docker/compose/releases/download/v2.17.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
docker-compose --version

echo "Installation terminée !"