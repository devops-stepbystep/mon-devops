# Mon Devops

This is a simple config.


Verify that `/usr/local/bin` is in the PATH:
If  `/usr/local/bin` is not part of your PATH. Add `/usr/local/bin` to your PATH with the following command:

```bash
export PATH=$PATH:/usr/local/bin
```

Add `/usr/local/bin` permanently to the PATH:
To make this change permanent, add the following line to your `~/.bashrc` or `~/.bash_profile` file (depending on your configuration):

```bash
echo 'export PATH=$PATH:/usr/local/bin' >> ~/.bashrc
source ~/.bashrc
```